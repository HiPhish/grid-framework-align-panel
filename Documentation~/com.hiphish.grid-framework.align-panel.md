# Alignment panel for Grid Framework

An editor extension which provides a new editor window that lets you align
object along a grid. You can either align selected objects only or all objects
in the scene. Or you can have object snap to the grid automatically as they are
being moved.


## Package contents

- A new editor window


## Installation instructions

Please refer to the official packages documentation on how to add a custom
package. I recommend adding this Git repository as a dependency to your project
and specifying the release tag you wish to use:

```json
{
  "dependencies": {
    "com.hiphish.grid-framework.align-panel": "https://gitlab.com/hiphish/grid-framework-align-panel.git#v3.0.0"
  }
}
```

You will need Grid Framework installed for the code to compile correctly.


## Requirements

- [Grid Framework](http://hiphish.github.io/grid-framework/)


## Getting started

In Unity's menu under "Window" there is a new entry called "Grid Align Panel".
This will open up a new window with the following fields:

- Grid: the grid instance
- Affected layers: which Unity3D layers to affect
- Rotate to grid: if set, then any aligned object will take on the rotation of
	the grid
- Ignore root objects: whether to ignore objects with children but no parent
- Include children: whether to align child objects
- Auto-Snapping: Snap objects as they are being dragged in the scene
- Ignore these axes: do not snap along the selected axes

To use it drag & drop an object with either a rectangular or hexagonal grid
from the hierarchy into the "Grid" field. The buttons are pretty self-
explanatory. You can also set which layers will be affected, this is especially
useful if you want to manipulate large groups of objects at the same time
without affecting the rest of the scene. If "Ignore Root Objects" is set "Align
Scene" and "Scale Scene" will ignore all objects that have no parent and at
least one child. If "Include Children" is not set then child objects will be
ignored.

Auto snapping makes all objects moved in editor mode snap automatically to the
grid. They will only snap if they have been selected, so you could turn this
option on and click on all the objects you want to align quickly and then turn
the option off again without affecting the other objects in the scene. You can
also set individual axes to not be affected by the aligning functions.


## Samples

- Playground: A static scene with a couple of grids and objects to play around
	with.
