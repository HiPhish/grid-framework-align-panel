# Alignment panel for Grid Framework

This editor extension offers a new panel which lets you quickly align objects
in the scene along a grid.


## Installation

This Git repository is a regular standalone Unity package. You can read more
about it in the [documentation].


## Usage

There will be a new menu item available under "Window" called "Grid Align
Panel". You can find a detailed explanation in the [documentation].


## License

Released under the MIT license. Please see the [LICENSE] file for details.



[documentation]: Documentation~/com.hiphish.grid-framework.vectrosity.md
